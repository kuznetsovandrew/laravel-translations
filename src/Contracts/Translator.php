<?php

namespace RedRay\LaravelTranslations\Contracts;

interface Translator
{
    public function translate(string $source, string $target, string $text);
}
