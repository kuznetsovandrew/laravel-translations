<?php

namespace RedRay\LaravelTranslations\Contracts;

interface LangFileAdapter
{
    public function exists(string $local, string $path): bool;

    public function read(string $local, string $path): array;

    public function write(string $local, string $path, array $data);

    public function getLanguageData(string $local, string $file): array;

    public function removeRedundant(array $originLanguageData, array $targetLanguageData): array;

    public function getDataForTranslation(array $originData, array $targetData, bool $override = false): array;
}
