<?php

namespace RedRay\LaravelTranslations\Commands;

use Illuminate\Console\Command;
use RedRay\LaravelTranslations\Contracts\LangFileAdapter;
use RedRay\LaravelTranslations\Contracts\Translator;

class Translate extends Command
{
    protected $signature = 'red-ray:translations:translate {local} {file} {targetLocals} {--override=false} {--removeRedundant=true}';

    protected $description = 'Translate file';

    private $translator;

    private $langFileAdapter;

    public function __construct(Translator $translator, LangFileAdapter $langFileAdapter)
    {
        parent::__construct();
        $this->translator = $translator;
        $this->langFileAdapter = $langFileAdapter;
    }

    public function handle()
    {
        $local = (string) $this->argument('local');
        $file = (string) $this->argument('file');

        if (! $this->langFileAdapter->exists($local, $file)) {
            $this->output->error("File doesn't exist: $file");

            return -1;
        }

        $originLanguageData = $this->langFileAdapter->getLanguageData($local, $file);

        $targetLocals = (string) $this->argument('targetLocals');
        $targetLocals = explode(',', trim($targetLocals, ','));

        $removeRedundant = filter_var($this->option('removeRedundant'), FILTER_VALIDATE_BOOLEAN);
        $override = filter_var($this->option('override'), FILTER_VALIDATE_BOOLEAN);

        foreach ($targetLocals as $targetLocal) {
            $targetLanguageData = $this->langFileAdapter->getLanguageData($targetLocal, $file);

            if ($removeRedundant) {
                $targetLanguageData = $this->langFileAdapter->removeRedundant($originLanguageData, $targetLanguageData);
            }

            $dataForTranslation = $this->langFileAdapter->getDataForTranslation(
                $originLanguageData,
                $targetLanguageData,
                $override
            );

            $this->output->title("Translation for local: $targetLocal file: $file");
            $bar = $this->output->createProgressBar(count($dataForTranslation));

            $translations = array_map(function ($value) use ($local, $targetLocal, $bar) {
                $bar->advance();

                if (!is_string($value)) {
                    return $value;
                }

                return $this->translator->translate($local, $targetLocal, $value);
            }, $dataForTranslation);

            $bar->finish();
            $this->output->newLine();

            // use this instead of array_merge() cause need to save keys sequence
            $result = collect($originLanguageData)->map(function ($val, $key) use ($translations, $targetLanguageData) {
                return $translations[$key] ?? $targetLanguageData[$key] ?? null;
            });

            $this->langFileAdapter->write($targetLocal, $file, $result->toArray());
        }

        $this->output->success('File was translated');

        return 0;
    }
}
