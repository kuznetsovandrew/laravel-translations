<?php

namespace RedRay\LaravelTranslations;

use Illuminate\Support\ServiceProvider;
use RedRay\LaravelTranslations\Commands\Translate;
use RedRay\LaravelTranslations\Contracts\LangFileAdapter as LangFileAdapterContract;
use RedRay\LaravelTranslations\Contracts\Translator as TranslatorContract;
use RedRay\LaravelTranslations\Library\LangFileAdapter;
use RedRay\LaravelTranslations\Library\Translator;

class LaravelTranslationsProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(TranslatorContract::class, Translator::class);
        $this->app->bind(LangFileAdapterContract::class, LangFileAdapter::class);
    }

    public function boot()
    {
        $this->commands([
            Translate::class,
        ]);
    }
}
