<?php

namespace RedRay\LaravelTranslations\Library;

use Dejurin\GoogleTranslateForFree;
use RedRay\LaravelTranslations\Contracts\Translator as TranslatorContract;

class Translator implements TranslatorContract
{
    protected $textLimit = 5000;

    public function translate(string $source, string $target, string $text)
    {
        return GoogleTranslateForFree::translate($source, $target, $text);
    }
}
