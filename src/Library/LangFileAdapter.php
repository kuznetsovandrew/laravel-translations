<?php

namespace RedRay\LaravelTranslations\Library;

use Illuminate\Support\Arr;
use RedRay\LaravelTranslations\Contracts\LangFileAdapter as LangFileAdapterContract;

class LangFileAdapter implements LangFileAdapterContract
{
    public function read(string $local, string $path): array
    {
        $filePath = $this->getFilepath($local, $path);

        if (! $this->exists($local, $path)) {
            throw new \InvalidArgumentException("File doesn't exist: $filePath");
        }

        return require $filePath;
    }

    public function exists(string $local, string $path): bool
    {
        $filePath = $this->getFilepath($local, $path);

        return file_exists($filePath) && is_file($filePath);
    }

    public function write(string $local, string $path, array $data)
    {
        $filepath = $this->getFilepath($local, $path);
        $dirPath = pathinfo($filepath)['dirname'];

        if (! file_exists($dirPath)) {
            mkdir($dirPath);
        }

        $writeData = [];

        foreach ($data as $key => $value) {
            Arr::set($writeData, $key, $value);
        }

        $resource = fopen($this->getFilepath($local, $path), 'w');

        try {
            fwrite($resource, '<?php'.PHP_EOL.PHP_EOL."return {$this->serializeArray($writeData)};");
        } catch (\Exception $exception) {
            throw $exception;
        } finally {
            fclose($resource);
        }
    }

    public function getLanguageData(string $local, string $file): array
    {
        return $this->exists($local, $file) ? Arr::dot($this->read($local, $file)) : [];
    }

    public function removeRedundant(array $originLanguageData, array $targetLanguageData): array
    {
        return array_intersect_key($targetLanguageData, $originLanguageData);
    }

    public function getDataForTranslation(array $originData, array $targetData, bool $override = false): array
    {
        if ($override) {
            return $originData;
        }

        return array_diff_key($originData, $targetData);
    }

    protected function getFilepath(string $local, string $path): string
    {
        return resource_path('lang'.DIRECTORY_SEPARATOR.$local.DIRECTORY_SEPARATOR.$path);
    }

    protected function serializeArray(array $expression): string
    {
        $export = var_export($expression, true);

        $export = preg_replace('/^([ ]*)(.*)/m', '$1$1$2', $export);

        $array = preg_split("/\r\n|\n|\r/", $export);

        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);

        return implode(PHP_EOL, array_filter(['['] + $array));
    }
}
