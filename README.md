#### Main purpose of the package it is speed up development of multilingual websites by automation of translation process.  

## Usage:

### php artisan red-ray:translations:translate ru admin.php en,uk --override=true --removeRedundant=true
#### Initial data:
    // ru/admin.php
    <?php
    
    return [
        'one' => 'Один',
        'two' => 'Два',
        'three' => 'Три',
    ];
    
#### as result of the command you will get two files (en/admin.php, uk/admin.php) identical to original one by structure:    
    // en/admin.php
    <?php
    
    return [
        'one' => 'One',
        'two' => 'Two',
        'three' => 'Three',
    ];

### php artisan red-ray:translations:translate ru admin.php en --override=false --removeRedundant=true
#### Initial data:
    // ru/admin.php
    <?php
    
    return [
        'one' => 'Один',
        'two' => 'Два',
    ];
    
    // en/admin.php
    <?php
    
    return [
        'one' => 'One',
        'two' => 'Two',
        'three' => 'Three',
    ];
    
#### The key "three" absents in the original language file, so it is redundant data, and it's gonna be removed:
    // en/admin.php
    <?php
    
    return [
        'one' => 'One',
        'two' => 'Two',
    ];

### php artisan red-ray:translations:translate ru admin.php en --override=true
#### Initial data:
    // ru/admin.php
    <?php
    
    return [
        'one' => 'Номер один',
    ];
    
    // en/admin.php
    <?php
    
    return [
        'one' => 'One',
    ];
#### Result:
    // en/admin.php
    <?php
    
    return [
        'one' => 'Number one',
    ];
